// Mandatory extern "C" headers and forward declarations
//#######################################################
#ifdef __cplusplus
extern "C" {
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_task.h"

#ifdef __cplusplus
}
#endif

#include "DInput5Relay2Module.hpp"
#include "EmodRet.hpp"
#include "EmodRetMng.hpp"
#include "HAL.hpp"

#include "di5pr2.hpp"

static const char *TAG = "di5pr2";

static DInput5Relay2Module di5pr2_module;
static TaskHandle_t relay_task;

// di5pr2_callback function
static void di5pr2_callback(const uint8_t* data, uint16_t datalen, uint8_t idFunction, void* ctx) {
    switch (idFunction) {
        case DInput5Relay2Module::idFunctionINPUTS:
        {
            bool inputs_status_array[DInput5Relay2Module::NUMBER_OF_DI_INPUTS];
            uint16_t inputs_status = data[1];
            for (int i = 0; i < DInput5Relay2Module::NUMBER_OF_DI_INPUTS; i++) {
                inputs_status_array[i] = (inputs_status & (1 << i)) != 0;
            }
            ESP_LOGI(TAG, "Inputs status: [%d, %d, %d, %d, %d]", inputs_status_array[0], inputs_status_array[1], inputs_status_array[2], inputs_status_array[3], inputs_status_array[4]);
        }
            break;
        default:
            ESP_LOGE(TAG, "Unexpected event %d", idFunction);
            break;
    }    
}

// relay_task_func function
static void relay_task_func(void *arg) {
    di5pr2_module.deactivateAll();

    for(;;) {
        for (int i = 0; i < DInput5Relay2Module::ALL_RELAY; i++) {
            di5pr2_module.activate(1 << i);
            vTaskDelay( 100 / portTICK_PERIOD_MS );
        }
        di5pr2_module.deactivateAll();
        vTaskDelay( 250 / portTICK_PERIOD_MS );
    }
}

// SetupRelay function
esp_err_t setupDI5PR2(void) {
    BaseType_t freertos_ret;

    if(di5pr2_module.init(di5pr2_callback, &di5pr2_module) != EmodRetOk){
        ESP_LOGE(TAG, "could not be initialized!");
		return -1;
	}

	uint32_t filter_time = 50;
	if(di5pr2_module.setPulseFilterTime(DInput5Relay2Module::DI_ALL_INPUT, filter_time) != EmodRetOk){
        ESP_LOGE(TAG, "could not configure input filter time!");
		return -1;
	}

    // Configure relays pulse width
	if(di5pr2_module.configPulseWidth(DInput5Relay2Module::ALL_RELAY, 0) != EmodRetOk){
        ESP_LOGE(TAG, "could not configure pulse width!");
		return -1;
	}

    sleepMs(100);

	if(di5pr2_module.resetEventConfig() != EmodRetOk){
        ESP_LOGE(TAG, "config fails, exiting!");
		return -1;
	}

	if(di5pr2_module.configEventOnNewData() != EmodRetOk){
        ESP_LOGE(TAG, "config fails, exiting!");
		return -1;
	}

    // Create relay_task_func task
    freertos_ret = xTaskCreate(relay_task_func, "relay_task", 4096, &di5pr2_module, ESP_TASK_MAIN_PRIO, &relay_task);
    if (freertos_ret != pdPASS) {
        return freertos_ret;
    }

    return ESP_OK;
}
