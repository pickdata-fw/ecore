#ifndef DI5PR2_HPP_
#define DI5PR2_HPP_

#include "esp_err.h"

esp_err_t setupDI5PR2(void);

#endif  // RELAY_HPP_