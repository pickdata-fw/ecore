# For more information about build system see
# https://docs.espressif.com/projects/esp-idf/en/latest/api-guides/build-system.html
# The following five lines of boilerplate have to be in your project's
# CMakeLists in this exact order for cmake to work correctly
cmake_minimum_required(VERSION 3.5)

include($ENV{IDF_PATH}/tools/cmake/project.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/../../cmake/download_emod_controller_component.cmake)
download_emod_controller_component(emod_controller_idf_v4.4_component-v2.5.1-0-gd12f7fdc.tar.gz 1e883a66bd9ef21df6a8885260b4f3d1)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project(ecore_relay8_module_test
    HOMEPAGE_URL https://bitbucket.org/pickdata-fw/ecore.git
    VERSION 3.1.1
    LANGUAGES C CXX
)
