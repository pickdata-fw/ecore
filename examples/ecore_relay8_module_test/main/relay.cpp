// Mandatory extern "C" headers and forward declarations
//#######################################################
#ifdef __cplusplus
extern "C" {
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_task.h"

#ifdef __cplusplus
}
#endif

#include "Relay8Module.hpp"
#include "EmodRet.hpp"
#include "EmodRetMng.hpp"

#include "relay.hpp"

static const char *TAG = "relay";

static Relay8Module relay;
static TaskHandle_t relay_task;

// relay_task_func function
static void relay_task_func(void *arg) {
    relay.deactivateAll();

    for(;;) {
        for (int i = 0; i < Relay8Module::NUMBER_OF_RELAYS; i++) {
            relay.activate(1 << i);
            vTaskDelay( 1000 / portTICK_PERIOD_MS );
        }
        relay.deactivateAll();
        vTaskDelay( 250 / portTICK_PERIOD_MS );
    }
}

// SetupRelay function
esp_err_t setupRelay(void) {
    BaseType_t freertos_ret;

    if(relay.init() != EmodRetOk){
        ESP_LOGE(TAG, "could not be initialized!");
		return -1;
	}

	if(relay.configPulseWidth(Relay8Module::ALL_RELAY, 0) != EmodRetOk){
        ESP_LOGE(TAG, "could not configure pulse width!");
		return -1;
	}

    freertos_ret = xTaskCreate(relay_task_func, "relay_task", 4096, &relay, ESP_TASK_MAIN_PRIO, &relay_task);
    if (freertos_ret != pdPASS) {
        return freertos_ret;
    }

    return ESP_OK;
}
