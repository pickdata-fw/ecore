#ifndef RELAY_HPP_
#define RELAY_HPP_

#include "esp_err.h"

esp_err_t setupRelay(void);

#endif  // RELAY_HPP_