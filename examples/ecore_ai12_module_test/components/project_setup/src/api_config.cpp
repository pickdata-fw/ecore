// extern "C" headers and forward declarations
/////////////////////////////////////////////////////////
#ifdef __cplusplus
extern "C"
{
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "esp_log.h"

#ifdef __cplusplus
}
#endif

#include "HAL.hpp"
#include "AdminApiServer.hpp"
#include "NetworkManager.hpp"
#include "EmodRet.hpp"

#include "api_config.hpp"

static const char *TAG = "api_config";

esp_err_t setupAdminApi(void) {
    // AdminApiServer init
    /////////////////////////////////////////////////////////
    AdminApiServer &api = AdminApiServer::getInstance();
    EmodRet ret = api.init();
    if (ret != EmodRetOk) {
        ESP_LOGE(TAG, "Error at AdminApiServer init");
        abort();
    }

    return ESP_OK;
}