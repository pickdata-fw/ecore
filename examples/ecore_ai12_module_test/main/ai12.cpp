// Mandatory extern "C" headers and forward declarations
//#######################################################
#ifdef __cplusplus
extern "C" {
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_task.h"

#ifdef __cplusplus
}
#endif

#include "AInput12Module.hpp"
#include "EmodRet.hpp"
#include "EmodRetMng.hpp"
#include "HAL.hpp"

#include "ai12.hpp"

static const char *TAG = "ai12";

static AInput12Module ai12_module;

static void ai12_callback(const uint8_t* data, uint16_t datalen, uint8_t idFunction, void* ctx) {
    AInput12Module* module = (AInput12Module*)ctx;
    uint16_t value;

    if(datalen == 2){
        value = (data[0] << 8) + data[1];
        ESP_LOGI(TAG, "Input %d --> voltage=%.2f current=%.2f", idFunction, module->samplesToVoltage(value), module->samplesToCurrent(value));
    }   
}

// SetupRelay function
esp_err_t setupAI12(void) {

    if(ai12_module.init(ai12_callback, &ai12_module) != EmodRetOk){
        ESP_LOGE(TAG, "could not be initialized!");
		return -1;
	}

    if(ai12_module.configSampleRate(250) != EmodRetOk){
        ESP_LOGE(TAG, "could not configure pulse width!");
		return -1;
	}

    sleepMs(100);

	if(ai12_module.resetEventConfig() != EmodRetOk){
        ESP_LOGE(TAG, "config fails, exiting!");
		return -1;
	}

	if(ai12_module.configEventAtTimeInterval(5000) != EmodRetOk){
        ESP_LOGE(TAG, "config fails, exiting!");
		return -1;
	}
    return ESP_OK;
}
