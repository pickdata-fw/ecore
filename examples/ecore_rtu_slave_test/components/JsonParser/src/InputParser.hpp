/****************************************************************************
 * File:    InputParser.hpp
 * Brief:   This file maps a json file into a structure.
 * Author:  Laia Seijas
 ***************************************************************************/
#ifndef INPUTPARSER_HPP_
#define INPUTPARSER_HPP_

#include <list>
#include <nlohmann/json.hpp>

#include "esp_err.h"

enum Aggregation
{
	AVERAGE,
	LAST,
};

struct Param
{
	std::string param_id;
	std::string description;
	std::string units;
	Aggregation aggregation;
	uint16_t multiplier;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Param, param_id, description, units, aggregation, multiplier);

struct Device
{
	uint16_t device_id;
	std::list<Param> params;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Device, device_id, params);

struct MapConfig
{
	std::list<Device> devices;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(MapConfig, devices);

struct TimeSlot
{
	bool is_full_green;
	uint32_t hours_mask;
	uint32_t max_power_w;
	uint16_t period;

	// Assignment operator
	///////////////////////////////////////////////////////////////////////////////////////////////
	TimeSlot operator=(const TimeSlot &ts)
	{
		TimeSlot p;
		p.hours_mask = ts.hours_mask;
		p.max_power_w = ts.max_power_w;
		p.is_full_green = ts.is_full_green;
		p.period = ts.period;
		return p;
	}
};
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(TimeSlot, is_full_green, hours_mask, max_power_w, period);

struct HolidayTimeSlot
{
	uint32_t max_power_w;
	bool is_full_green;
	uint16_t period;
};
NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(HolidayTimeSlot, max_power_w, is_full_green, period);

struct TimeSlots
{
	std::list<TimeSlot> time_slots;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(TimeSlots, time_slots);

struct ManualMode
{
	bool is_boost_mode;
	bool is_enabled;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ManualMode, is_boost_mode, is_enabled);

struct Current
{
	int32_t min_current_ma;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Current, min_current_ma);

struct ChargerCurrent
{
	uint32_t max_current_a;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ChargerCurrent, max_current_a);

struct FullScaleConfig
{
	uint16_t current_fs1;
	uint16_t current_fs2;
	uint16_t current_fs3;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(FullScaleConfig, current_fs1, current_fs2, current_fs3);

struct ZeroBalance
{
	bool zero_balance_activated;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ZeroBalance, zero_balance_activated);

struct Config
{
	TimeSlots timeslots;
	HolidayTimeSlot holiday_config;
	Current current;
	ManualMode manual_mode;
	FullScaleConfig full_scale;
	ChargerCurrent charger_current;
	ZeroBalance zero_balance;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Config, timeslots, holiday_config, current, manual_mode, full_scale, charger_current, zero_balance);

struct Emeter3PhasePower
{
	uint16_t phase;
	int32_t current_ma;
	int32_t power_w;
	int32_t maximeter_w;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Emeter3PhasePower, phase, current_ma, power_w, maximeter_w);

struct Emeter3PhaseEnergy
{
	uint16_t phase;
	int32_t energy_kwh;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Emeter3PhaseEnergy, phase, energy_kwh);

struct PowerReport
{
	std::list<Emeter3PhasePower> emeter3_power;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(PowerReport, emeter3_power);

struct EnergyReport
{
	std::list<Emeter3PhaseEnergy> emeter3_energy;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(EnergyReport, emeter3_energy);

struct StatusReport
{
	std::string charger_status;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(StatusReport, charger_status);

struct Report
{
	StatusReport status_report;
	EnergyReport energy_report;
	PowerReport power_report;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Report, status_report, energy_report, power_report);

struct Timestamp
{
	std::string timestamp;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Timestamp, timestamp);

struct InvertedCurrent
{
	bool is_inverted_p1;
	bool is_inverted_p2;
	bool is_inverted_p3;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(InvertedCurrent, is_inverted_p1, is_inverted_p2, is_inverted_p3);

struct Configuration
{
	FullScaleConfig full_scale;
	Current current;
	ChargerCurrent charger_current;
	InvertedCurrent is_inverted;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Configuration, full_scale, current, charger_current, is_inverted);

#endif // MODBUS_HPP_