/****************************************************************************
 * File:    emeter3.hpp
 * Brief:   This module establish the communication with the eMeter3 module.
 * Author:  Laia Seijas
 ***************************************************************************/
#ifndef EMETER3_HPP_
#define EMETER3_HPP_

#include <mutex>

#include "EnergyMeter3Module.h"
#include "EnergyMeter3Module.hpp"
#include "InputParser.hpp"
#include "esp_err.h"

/**
 * This class interacts with the Emeter3 module.
 *
 */
class Emeter3 {
private:
	// Emeter3 module attributes
	///////////////////////////////////////////////////////////////////////////////////////////////
	EnergyMeter3Module emeter3_module_;
	bool is_initialized_;
	bool is_inverted_p1_;
	bool is_inverted_p2_;
	bool is_inverted_p3_;

	// Full scale configuration attributes
	///////////////////////////////////////////////////////////////////////////////////////////////
	uint16_t current_fs1_;
	uint16_t current_fs2_;
	uint16_t current_fs3_;

	// Variables protection
	///////////////////////////////////////////////////////////////////////////////////////////////
	mutable std::mutex mutex_;

	// Other atributes
	///////////////////////////////////////////////////////////////////////////////////////////////
	int32_t voltage_em3_;

	// Constants
	///////////////////////////////////////////////////////////////////////////////////////////////
	static const int32_t kWorkMode = 1;
	static const int32_t kMeteringStandard = 1;
	static const int32_t kVoltageFs = 230;

	// Setters
	///////////////////////////////////////////////////////////////////////////////////////////////
	void setCurrentFs1(const uint16_t current_fs1);
	void setCurrentFs2(const uint16_t current_fs2);
	void setCurrentFs3(const uint16_t current_fs3);

	// Getters
	///////////////////////////////////////////////////////////////////////////////////////////////
	uint16_t getCurrentFs1() const;
	uint16_t getCurrentFs2() const;
	uint16_t getCurrentFs3() const;

	// Constructor, destructor and methods deleted for a singletone class
	///////////////////////////////////////////////////////////////////////////////////////////////
	Emeter3();
	~Emeter3();
	Emeter3(const Emeter3&) = delete;
	Emeter3& operator=(const Emeter3&) = delete;

public:
	// Instantiate the Emeter3
	///////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Returns an intsance of the Emeter3 class in order to have a singletone.
	 *
	 */
	static Emeter3& getInstance();

	// Setters
	///////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * It configures the voltage and current full scale configurations.
	 *
	 */
	esp_err_t setupFullScale();

	/**
	 * It configures and initializes the emeter3 module.
	 *
	 */
	esp_err_t setupEmeter3();

	/**
	 * It maps the full scale configuration in the structure into the attributes.
	 *
	 * @param[in] fs_config: contains the full scale configuration.
	 */
	void setFullScaleConfig(const FullScaleConfig fs_config);

	/**
	 * Set if the module is initialized or not
	 *
	 * @param[in] is_initialized: set if the module has been properly initialized or not.
	 *
	 */
	void setIsInitialized(const bool is_initialized);

	void setVoltage(const int32_t voltage);

	// Getters
	///////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Reads the emeter3 power parameters
	 *
	 * @param[in] id_func: id of the line that we want to obtain (L1, L2, L3).
	 * @param[in] power_params: register to store the values read.
	 *
	 */
	esp_err_t getPowerParametersEmeter3(const int id_func, PhasePowerParameters* power_params);

	/**
	 * Reads the emeter3 power parameters
	 *
	 * @param[in] id_func: id of the line that we want to obtain (L1, L2, L3).
	 * @param[in] power_params: register to store the values read.
	 *
	 */
	esp_err_t getEnergyParametersEmeter3(const int id_func, EnergyParameters* energy_params);

	/**
	 * It returns the structure that contains all the full scale configurations.
	 *
	 */
	FullScaleConfig getFullScaleConfig();

	/**
	 * It returns if the module has been initialized properly
	 *
	 */
	bool getIsInitialized() const;

	int32_t getVoltage() const;

	bool getIsInvertedPhase1() const;

	void setIsInvertedPhase1(const bool is_inverted);

	bool getIsInvertedPhase2() const;

	void setIsInvertedPhase2(const bool is_inverted);

	bool getIsInvertedPhase3() const;

	void setIsInvertedPhase3(const bool is_inverted);
};

#endif	// EMETER3_HPP_