// Mandatory extern "C" headers and forward declarations
//#######################################################
#ifdef __cplusplus
extern "C" {
#endif

// ESP-IDF FreeRTOS headers
#include "esp_log.h"
#include "esp_task.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#ifdef __cplusplus
}
#endif

#include <endian.h>

#include "EmodRet.hpp"
#include "EmodRetMng.hpp"
#include "Sleep.h"
#include "emeter3.hpp"

static const char* EMETER3_TAG = "Emeter3";

// Constructor & Destructor
///////////////////////////////////////////////////////////////////////////////////////////////
Emeter3::Emeter3()
	: is_initialized_(false),
	  is_inverted_p1_(false),
	  is_inverted_p2_(false),
	  is_inverted_p3_(false),
	  current_fs1_(1000),
	  current_fs2_(1000),
	  current_fs3_(1000),
	  voltage_em3_(230) {}

Emeter3::~Emeter3() {}

// Setters
///////////////////////////////////////////////////////////////////////////////////////////////
esp_err_t Emeter3::setupFullScale() {
	if (emeter3_module_.configCurrentFullScale(current_fs1_, current_fs2_, current_fs3_) != EmodRetOk) {
		ESP_LOGE(EMETER3_TAG, "Current full scale could not be configured!");
		return -1;
	}

	if (emeter3_module_.configVoltageFullScale(kVoltageFs, kVoltageFs, kVoltageFs) != EmodRetOk) {
		ESP_LOGE(EMETER3_TAG, "Voltage full scale could not be configured!");
		return -1;
	}

	ESP_LOGI(EMETER3_TAG, "Current full scale configured properly!");
	return ESP_OK;
}

esp_err_t Emeter3::setupEmeter3() {
	if (emeter3_module_.init(ModuleType::typeEM3) != EmodRetOk) {
		ESP_LOGE(EMETER3_TAG, "Error, module could not be initialized!");
		return -1;
	}

	if (setupFullScale() != EmodRetOk) {
		return -1;
	}

	if (emeter3_module_.configWorkMode((uint16_t)kWorkMode) != EmodRetOk) {
		ESP_LOGE(EMETER3_TAG, "Voltage full scale could not be configured!");
		return -1;
	}

	if (emeter3_module_.configMeteringStandard((uint16_t)kMeteringStandard) != EmodRetOk) {
		ESP_LOGE(EMETER3_TAG, "Voltage full scale could not be configured!");
		return -1;
	}

	emeter3_module_.resetAllEnergyMeter();

	return ESP_OK;
}

void Emeter3::setCurrentFs1(const uint16_t current_fs1) { current_fs1_ = current_fs1; }
void Emeter3::setCurrentFs2(const uint16_t current_fs2) { current_fs2_ = current_fs2; }
void Emeter3::setCurrentFs3(const uint16_t current_fs3) { current_fs3_ = current_fs3; }

void Emeter3::setFullScaleConfig(const FullScaleConfig fs_config) {
	const std::lock_guard<std::mutex> lock(mutex_);
	setCurrentFs1(fs_config.current_fs1);
	setCurrentFs2(fs_config.current_fs2);
	setCurrentFs3(fs_config.current_fs3);

	if (is_initialized_ == true) {
		setupFullScale();
	}
	// ESP_LOGI(EMETER3_TAG, "fs1: %u, fs2: %u, fs3: %u, fs: %u",getCurrentFs1(),getCurrentFs2(),getCurrentFs3(), getVoltageFs());
}

void Emeter3::setIsInitialized(const bool is_initialized) {
	const std::lock_guard<std::mutex> lock(mutex_);
	is_initialized_ = is_initialized;
}

void Emeter3::setVoltage(const int32_t voltage) {
	const std::lock_guard<std::mutex> lock(mutex_);
	voltage_em3_ = voltage;
}

void Emeter3::setIsInvertedPhase1(const bool is_inverted) {
	const std::lock_guard<std::mutex> lock(mutex_);
	is_inverted_p1_ = is_inverted;
}

void Emeter3::setIsInvertedPhase2(const bool is_inverted) {
	const std::lock_guard<std::mutex> lock(mutex_);
	is_inverted_p2_ = is_inverted;
}

void Emeter3::setIsInvertedPhase3(const bool is_inverted) {
	const std::lock_guard<std::mutex> lock(mutex_);
	is_inverted_p3_ = is_inverted;
}

// Getters
///////////////////////////////////////////////////////////////////////////////////////////////
Emeter3& Emeter3::getInstance() {
	static Emeter3 instance;
	return instance;
}

bool Emeter3::getIsInvertedPhase1() const {
	const std::lock_guard<std::mutex> lock(mutex_);
	return is_inverted_p1_;
}

bool Emeter3::getIsInvertedPhase2() const {
	const std::lock_guard<std::mutex> lock(mutex_);
	return is_inverted_p2_;
}

bool Emeter3::getIsInvertedPhase3() const {
	const std::lock_guard<std::mutex> lock(mutex_);
	return is_inverted_p3_;
}

esp_err_t Emeter3::getEnergyParametersEmeter3(const int id_func, EnergyParameters* energy_params) {
	if (id_func == 1) {
		if (emeter3_module_.getEnergyParameters_L1(energy_params) != EmodRetOk) {
			return -1;
		}
	}
	if (id_func == 2) {
		if (emeter3_module_.getEnergyParameters_L2(energy_params) != EmodRetOk) {
			return -1;
		}
	}
	if (id_func == 3) {
		if (emeter3_module_.getEnergyParameters_L3(energy_params) != EmodRetOk) {
			return -1;
		}
	}

	/*printf("\n");
	printf("Energy-Phase-L%d                         ActiveEnergy=%010.3f kWh   AparentEnergy=%010.3f kVAh   ImportedEnergy=%010.3f kWh ExportedEnergy=%010.3f
	kWh\n", id_func, energy_params->active_energy, energy_params->aparent_energy, energy_params->imported_active_energy, energy_params->exported_active_energy);
*/
	return ESP_OK;
}

esp_err_t Emeter3::getPowerParametersEmeter3(int id_func, PhasePowerParameters* power_params) {
	if (id_func == 1) {
		if (emeter3_module_.getPowerParameters_L1(power_params) != EmodRetOk) {
			return -1;
		}
	}
	if (id_func == 2) {
		if (emeter3_module_.getPowerParameters_L2(power_params) != EmodRetOk) {
			return -1;
		}
	}
	if (id_func == 3) {
		if (emeter3_module_.getPowerParameters_L3(power_params) != EmodRetOk) {
			return -1;
		}
	}

	/*printf("\n");
	printf("Power-Phase-L%d    Voltage=%010.3f V  Current    =%010.3f A    Frequency   =%010.3f Hz    Cosine        =%010.3f        Angle          =%010.3f
	º\n", id_func, power_params->voltage, power_params->current, power_params->frequency, power_params->cosine, power_params->angle); printf("
	ActivePower=%010.3f kW   AparentPower=%010.3f kVA   ReactivePower =%010.3f kVAr\n", power_params->active_power, power_params->aparent_power,
	power_params->reactive_power);
*/
	return ESP_OK;
}

uint16_t Emeter3::getCurrentFs1() const { return current_fs1_; }
uint16_t Emeter3::getCurrentFs2() const { return current_fs2_; }
uint16_t Emeter3::getCurrentFs3() const { return current_fs3_; }

int32_t Emeter3::getVoltage() const {
	const std::lock_guard<std::mutex> lock(mutex_);
	return voltage_em3_;
}

FullScaleConfig Emeter3::getFullScaleConfig() {
	FullScaleConfig fs_config;
	const std::lock_guard<std::mutex> lock(mutex_);

	fs_config.current_fs1 = getCurrentFs1();
	fs_config.current_fs2 = getCurrentFs2();
	fs_config.current_fs3 = getCurrentFs3();

	// ESP_LOGI(EMETER3_TAG, "fs1: %u, fs2: %u, fs3: %u, fs: %u",fs_config.current_fs1,fs_config.current_fs2,fs_config.current_fs3,fs_config.voltage_fs);

	return fs_config;
}

bool Emeter3::getIsInitialized() const {
	const std::lock_guard<std::mutex> lock(mutex_);
	return is_initialized_;
}
