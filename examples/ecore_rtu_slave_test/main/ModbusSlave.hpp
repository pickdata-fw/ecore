/****************************************************************************
 * File:    ModbusSlave.hpp
 * Brief:   This class is able to act as a modbus slave
 * Author:  Laia Seijas
 ***************************************************************************/
#ifndef MODBUSSLAVE_HPP_
#define MODBUSSLAVE_HPP_

#include <cstring>
#include <iostream>
#include <map>
#include <string>

#include "HAL.hpp"
#include "driver/uart.h"
#include "esp_err.h"
#include "mbcontroller.h"
#include "modbus_params.h"
/**
 * This class implements the modbus protocol for a slave device.
 *
 */
class ModbusSlave
{
private:
	uint32_t baudrate_;
	uint16_t parity_;
	holding_reg_params_t holding_reg_params_;
	input_reg_params_t input_reg_params_;
	coil_reg_params_t coil_reg_params_;
	discrete_reg_params_t discrete_reg_params_;

	void *mbc_slave_handler_;

	mb_param_info_t reg_info_;				 // keeps the Modbus registers access information
	mb_communication_info_t comm_;			 // Modbus communication parameters
	mb_register_area_descriptor_t reg_area_; // Modbus register area descriptor structure

	TaskHandle_t modbus_task_;
	void *modbus_task_stack_;
	StaticTask_t *modbus_task_buffer_;

	uint16_t mode_;

	// Constants
	///////////////////////////////////////////////////////////////////////////////////////////////
	static const int32_t kMbPortUart = UART_NUM_1;
	static const int32_t kConfigMbUartTxd = 13;
	static const int32_t kConfigMbUartRxd = 14;
	static const int32_t kConfigMbUartRts = 12;
	static const int32_t kSlaveAdress = 72;

	// Constructor, destructor and init methods
	///////////////////////////////////////////////////////////////////////////////////////////////
	ModbusSlave();
	~ModbusSlave();
	ModbusSlave(const ModbusSlave &) = delete;
	ModbusSlave &operator=(const ModbusSlave &) = delete;

	static void modbusTaskFunc(void *arg);

	void initRTU();
	void initTCP();

public:
	// Constants enum for modbus mode
	///////////////////////////////////////////////////////////////////////////////////////////////
	enum ConnMode : uint16_t
	{
		kTCP = 1,
		kRTU = 2,
	};

	enum AddrType : uint16_t
	{
		kIPV4 = 1,
		kIPV6 = 2,
	};

	enum Parity : uint16_t
	{
		kEven = 1,
		kOdd = 2,
		kNone = 3,
	};

	static ModbusSlave &getInstance();
	void setup();
	void init();
	void initRegisters();
	void start();
	void setMode(const ConnMode mode);
	void setBaudrate(const uint32_t baudrate);
	void setParity(const Parity parity);
	void setVoltage(const int id, const uint16_t voltage);
};

#endif // MODBUS_HPP_