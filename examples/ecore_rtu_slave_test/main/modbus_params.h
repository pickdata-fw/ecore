/*=====================================================================================
 * Description:
 *   The Modbus parameter structures used to define Modbus instances that
 *   can be addressed by Modbus protocol. Define these structures per your needs in
 *   your application. Below is just an example of possible parameters.
 *====================================================================================*/
#ifndef _DEVICE_PARAMS
#define _DEVICE_PARAMS

// This file defines structure of modbus parameters which reflect correspond modbus address space
// for each modbus register type (coils, discreet inputs, holding registers, input registers)
#pragma pack(push, 1)
typedef struct
{
    uint8_t discrete_input0 : 1;
    uint8_t discrete_input1 : 1;
    uint8_t discrete_input2 : 1;
    uint8_t discrete_input3 : 1;
    uint8_t discrete_input4 : 1;
    uint8_t discrete_input5 : 1;
    uint8_t discrete_input6 : 1;
    uint8_t discrete_input7 : 1;
    uint8_t discrete_input_port1 : 8;
} discrete_reg_params_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
    uint8_t coils_port0;
    uint8_t coils_port1;
} coil_reg_params_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
    uint16_t voltageL1;   // 0
    uint16_t voltageL2;   // 2
    uint16_t voltageL3;   // 4
    uint16_t input_data3; // 6
    uint16_t input_data4; // 8
    uint16_t input_data5;
    uint16_t input_data6;
    uint16_t input_data7;
} input_reg_params_t;
#pragma pack(pop)

#pragma pack(push, 1)
typedef struct
{
    uint16_t holding_data0;
    uint16_t holding_data1;
    uint16_t holding_data2;
    uint16_t holding_data3;
    uint16_t holding_data4;
    uint16_t holding_data5;
    uint16_t holding_data6;
    uint16_t holding_data7;
} holding_reg_params_t;
#pragma pack(pop)

#endif // !defined(_DEVICE_PARAMS)
