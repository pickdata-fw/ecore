// Mandatory extern "C" headers and forward declarations
//#######################################################
#ifdef __cplusplus
extern "C"
{
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "mbcontroller.h"
#include "EnergyMeter3Module.h"

    void app_main();

#ifdef __cplusplus
}
#endif

// Mandatory headers
//#######################################################
#include "api_config.hpp"
#include "serial_log_config.hpp"
#include <remote_log.h>
#include "ModbusSlave.hpp"
#include "emeter3.hpp"

// Put your consts here
/////////////////////////////////////////////////////////
static const char *TAG = "SLAVE_TEST";

ModbusSlave &modbus_slave = ModbusSlave::getInstance();

// Mandatory app_main function
//#######################################################
void app_main(void)
{
    // Mandatory calls
    //#######################################################
    ESP_ERROR_CHECK(setupAdminApi());
    ESP_LOGI(TAG, "Admin API configured!");

    ESP_ERROR_CHECK(setupSerialLog());
    ESP_LOGI(TAG, "Serial Log configured!");

    //This function enables telnet logs. You can find more information in https://docs.pickdata.net/ecore/setup/#telnet-logs-configuration
    // ESP_ERROR_CHECK(remoteLogInit());
    // ESP_LOGI(TAG, "Remote log configured!");

    // Put your code here
    /////////////////////////////////////////////////////////

    // Set UART log level
    esp_log_level_set(TAG, ESP_LOG_INFO);

    modbus_slave.setMode(ModbusSlave::kRTU);
    modbus_slave.setParity(ModbusSlave::kNone);
    modbus_slave.setBaudrate(9600);
    modbus_slave.init();
    modbus_slave.setup();
    modbus_slave.start();

    // The cycle below will be terminated when parameter holdingRegParams.dataChan0
    // incremented each access cycle reaches the CHAN_DATA_MAX_VAL value.
    uint32_t i = 0;

    while (1)
    {
        modbus_slave.setVoltage(1, i);
        HAL::sleepS(1);
        modbus_slave.setVoltage(2, i);
        HAL::sleepS(1);
        modbus_slave.setVoltage(3, i);
        i = i + 1;
        HAL::sleepS(1);
    }
}
