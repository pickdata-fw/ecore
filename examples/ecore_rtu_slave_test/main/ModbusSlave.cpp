#ifdef __cplusplus
extern "C"
{
#endif

// ESP-IDF FreeRTOS headers
#include "esp_log.h"
#include "esp_task.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#ifdef __cplusplus
}
#endif

#include "EmodRet.hpp"
#include "EmodRetMng.hpp"
#include "ModbusSlave.hpp"

//#include "IDFNetworkEthernetInterface.hpp"

static const char *TAG = "ModbusSlave";

// Defines below are used to define register start address for each type of Modbus registers
#define HOLD_OFFSET(field) ((uint16_t)(offsetof(holding_reg_params_t, field) >> 1))
#define INPUT_OFFSET(field) ((uint16_t)(offsetof(input_reg_params_t, field) >> 1))
#define MB_REG_DISCRETE_INPUT_START (0x0000)
#define MB_REG_COILS_START (0x0000)
#define MB_REG_INPUT_START_AREA0 (INPUT_OFFSET(voltageL1))	 // register offset input area 0
#define MB_REG_INPUT_START_AREA1 (INPUT_OFFSET(input_data4)) // register offset input area 1
#define MB_REG_HOLDING_START_AREA0 (HOLD_OFFSET(holding_data0))
#define MB_REG_HOLDING_START_AREA1 (HOLD_OFFSET(holding_data4))

#define MB_PAR_INFO_GET_TOUT (10) // Timeout for get parameter info
#define MB_CHAN_DATA_MAX_VAL (6)
#define MB_READ_MASK (MB_EVENT_INPUT_REG_RD | MB_EVENT_HOLDING_REG_RD | MB_EVENT_DISCRETE_RD | MB_EVENT_COILS_RD)
#define MB_WRITE_MASK (MB_EVENT_HOLDING_REG_WR | MB_EVENT_COILS_WR)
#define MB_READ_WRITE_MASK (MB_READ_MASK | MB_WRITE_MASK)

// Constructors, Destructor & Init
///////////////////////////////////////////////////////////////////////////////////////////////
ModbusSlave::ModbusSlave() : baudrate_(9600), parity_(ModbusSlave::kNone), mbc_slave_handler_(nullptr), mode_(ModbusSlave::kRTU)
{
}

ModbusSlave::~ModbusSlave()
{
	if (modbus_task_stack_ != nullptr)
	{
		heap_caps_free(modbus_task_stack_);
	}
	if (modbus_task_buffer_ != nullptr)
	{
		heap_caps_free(modbus_task_buffer_);
	}
	ESP_LOGI(TAG, "Modbus controller destroyed.");
	ESP_ERROR_CHECK(mbc_slave_destroy());
}

ModbusSlave &ModbusSlave::getInstance()
{
	static ModbusSlave instance;
	return instance;
}

void ModbusSlave::init()
{
	// Check if Modbus slave must be initialized in TCP or RTU mode
	if (mode_ == kTCP)
	{
		initTCP();
	}
	if (mode_ == kRTU)
	{
		initRTU();
	}
}

void ModbusSlave::setMode(const ConnMode mode) { mode_ = mode; }

void ModbusSlave::setBaudrate(const uint32_t baudrate) { baudrate_ = baudrate; }

void ModbusSlave::setParity(const Parity parity) { parity_ = parity; }

void ModbusSlave::initRTU()
{
	ESP_ERROR_CHECK(mbc_slave_init(MB_PORT_SERIAL_SLAVE, &mbc_slave_handler_)); // Initialization of Modbus controller

	comm_.mode = MB_MODE_RTU,
	comm_.slave_addr = kSlaveAdress;
	comm_.port = kMbPortUart;
	comm_.baudrate = baudrate_;
	if (parity_ == kNone)
	{
		comm_.parity = MB_PARITY_NONE;
	}
	else if (parity_ == kEven)
	{
		comm_.parity = UART_PARITY_EVEN;
	}
	else if (parity_ == kOdd)
	{
		comm_.parity = UART_PARITY_ODD;
	}
	ESP_ERROR_CHECK(mbc_slave_setup((void *)&comm_));
}

void ModbusSlave::initTCP()
{
	/*NetworkManager &nm = HAL::getNetworkManager();

	if (nm.init() != ESP_OK)
	{
		ESP_LOGE(MASTER_TAG, "Error init NetworkManager");
	}
	auto eth = nm.findEthernetInterface("eth");
	if (eth == NULL)
	{
		ESP_LOGE(MASTER_TAG, "Error eth interface");
	}

	auto eth2 = std::dynamic_pointer_cast<IDFNetworkEthernetInterface>(eth);
	if (eth2 == NULL)
	{
		ESP_LOGE(MASTER_TAG, "Error eth2 interface");
	}

	if (eth2->getNetIf() == NULL)
	{
		ESP_LOGE(MASTER_TAG, "Error netif NULL interface");
	}
*/
	// Initialization of Modbus controller
	esp_err_t err = mbc_slave_init_tcp(&mbc_slave_handler_);
	if (err != ESP_OK || mbc_slave_handler_ == NULL)
	{
		ESP_LOGE(TAG, "mb controller initialization fail.");
	}
	std::string ip_a = "10.1.10.26";
	char *ip = new char[ip_a.length() + 1];
	std::strcpy(ip, ip_a.c_str());
	comm_.ip_addr = ip; // Bind to any address
	// comm_->ip_netif_ptr = eth2->getNetIf();
	comm_.ip_netif_ptr = NULL;
	comm_.ip_addr_type = MB_IPV4;
	comm_.ip_mode = MB_MODE_TCP;
	comm_.ip_port = 1502;

	// Setup communication parameters and start stack
	err = mbc_slave_setup((void *)&comm_);
	if (err != ESP_OK)
	{
		ESP_LOGE(TAG, "mbc_slave_setup fail, returns(0x%x).", (uint32_t)err);
	}
}

void ModbusSlave::setup()
{

	// The code below initializes Modbus register area descriptors
	// for Modbus Holding Registers, Input Registers, Coils and Discrete Inputs
	// Initialization should be done for each supported Modbus register area according to register map.
	// When external master trying to access the register in the area that is not initialized
	// by mbc_slave_set_descriptor() API call then Modbus stack
	// will send exception response for this register area.
	reg_area_.type = MB_PARAM_HOLDING;								// Set type of register area
	reg_area_.start_offset = MB_REG_HOLDING_START_AREA0;			// Offset of register area in Modbus protocol
	reg_area_.address = (void *)&holding_reg_params_.holding_data0; // Set pointer to storage instance
	// Set the size of register storage instance = 150 holding registers
	reg_area_.size = sizeof(holding_reg_params_);
	ESP_ERROR_CHECK(mbc_slave_set_descriptor(reg_area_));

	// Initialization of Input Registers area
	reg_area_.type = MB_PARAM_INPUT;
	reg_area_.start_offset = MB_REG_INPUT_START_AREA0;
	reg_area_.address = (void *)&input_reg_params_.voltageL1;
	reg_area_.size = sizeof(input_reg_params_);
	ESP_ERROR_CHECK(mbc_slave_set_descriptor(reg_area_));

	initRegisters(); // Set values into known state
}

void ModbusSlave::initRegisters()
{

	holding_reg_params_.holding_data0 = 100;
	holding_reg_params_.holding_data1 = 200;
	holding_reg_params_.holding_data2 = 300;
	holding_reg_params_.holding_data3 = 400;

	holding_reg_params_.holding_data4 = 500;
	holding_reg_params_.holding_data5 = 600;
	holding_reg_params_.holding_data6 = 700;
	holding_reg_params_.holding_data7 = 800;

	input_reg_params_.voltageL1 = 0;
	input_reg_params_.voltageL2 = 0;
	input_reg_params_.voltageL3 = 0;
	input_reg_params_.input_data3 = 600;

	input_reg_params_.input_data4 = 700;
	input_reg_params_.input_data5 = 800;
	input_reg_params_.input_data6 = 900;
	input_reg_params_.input_data7 = 100;
}

void ModbusSlave::start()
{
	// Starts of modbus controller and stack
	ESP_ERROR_CHECK(mbc_slave_start());

	if (mode_ == kRTU)
	{

		// Set UART pin numbers
		ESP_ERROR_CHECK(uart_set_pin(kMbPortUart, kConfigMbUartTxd,
									 kConfigMbUartRxd, kConfigMbUartRts,
									 UART_PIN_NO_CHANGE));

		// Set UART driver mode to Half Duplex
		ESP_ERROR_CHECK(uart_set_mode(kMbPortUart, UART_MODE_RS485_HALF_DUPLEX));
	}

	ESP_LOGI(TAG, "Modbus slave stack initialized.");
	ESP_LOGI(TAG, "Start modbus test...");

	modbus_task_buffer_ = (StaticTask_t *)heap_caps_calloc(1, sizeof(StaticTask_t), (MALLOC_CAP_INTERNAL | MALLOC_CAP_8BIT));
	modbus_task_stack_ = heap_caps_calloc(1, CONFIG_PTHREAD_TASK_STACK_SIZE_DEFAULT, (MALLOC_CAP_SPIRAM | MALLOC_CAP_8BIT));
	if (modbus_task_buffer_ == nullptr)
	{
		ESP_LOGE(TAG, "Error getting memory for modbus_task_buffer_");
	}

	if (modbus_task_stack_ == nullptr)
	{
		ESP_LOGE(TAG, "Error getting memory for modbus_task_stack_");
	}
	modbus_task_ = xTaskCreateStatic(ModbusSlave::modbusTaskFunc, "modbus_task", CONFIG_PTHREAD_TASK_STACK_SIZE_DEFAULT, this, ESP_TASK_MAIN_PRIO,
									 (StackType_t *)modbus_task_stack_, modbus_task_buffer_);
}

void ModbusSlave::modbusTaskFunc(void *arg)
{
	ModbusSlave *modbus = (ModbusSlave *)arg;

	ESP_LOGI(TAG, "Starting modbus task...");

	for (;;)
	{
		// Check for read/write events of Modbus master for certain events
		mb_event_group_t event = mbc_slave_check_event((mb_event_group_t)MB_READ_WRITE_MASK);
		const char *rw_str = (event & MB_READ_MASK) ? "READ" : "WRITE";

		// Filter events and process them accordingly
		if (event & (MB_EVENT_HOLDING_REG_WR | MB_EVENT_HOLDING_REG_RD))
		{
			// Get parameter information from parameter queue
			ESP_ERROR_CHECK(mbc_slave_get_param_info(&(modbus->reg_info_), MB_PAR_INFO_GET_TOUT));
			ESP_LOGI(TAG, "HOLDING %s (%u us), ADDR:%u, TYPE:%u, INST_ADDR:0x%.4x, SIZE:%u",
					 rw_str,
					 (uint32_t)modbus->reg_info_.time_stamp,
					 (uint32_t)modbus->reg_info_.mb_offset,
					 (uint32_t)modbus->reg_info_.type,
					 (uint32_t)modbus->reg_info_.address,
					 (uint32_t)modbus->reg_info_.size);
		}
		else if (event & MB_EVENT_INPUT_REG_RD)
		{
			ESP_ERROR_CHECK(mbc_slave_get_param_info(&(modbus->reg_info_), MB_PAR_INFO_GET_TOUT));
			ESP_LOGI(TAG, "INPUT READ (%u us), ADDR:%u, TYPE:%u, INST_ADDR:0x%.4x, SIZE:%u",
					 (uint32_t)modbus->reg_info_.time_stamp,
					 (uint32_t)modbus->reg_info_.mb_offset,
					 (uint32_t)modbus->reg_info_.type,
					 (uint32_t)modbus->reg_info_.address,
					 (uint32_t)modbus->reg_info_.size);
		}
		else if (event & MB_EVENT_DISCRETE_RD)
		{
			ESP_ERROR_CHECK(mbc_slave_get_param_info(&(modbus->reg_info_), MB_PAR_INFO_GET_TOUT));
			ESP_LOGI(TAG, "DISCRETE READ (%u us): ADDR:%u, TYPE:%u, INST_ADDR:0x%.4x, SIZE:%u",
					 (uint32_t)modbus->reg_info_.time_stamp,
					 (uint32_t)modbus->reg_info_.mb_offset,
					 (uint32_t)modbus->reg_info_.type,
					 (uint32_t)modbus->reg_info_.address,
					 (uint32_t)modbus->reg_info_.size);
		}
		else if (event & (MB_EVENT_COILS_RD | MB_EVENT_COILS_WR))
		{
			ESP_ERROR_CHECK(mbc_slave_get_param_info(&(modbus->reg_info_), MB_PAR_INFO_GET_TOUT));
			ESP_LOGI(TAG, "COILS %s (%u us), ADDR:%u, TYPE:%u, INST_ADDR:0x%.4x, SIZE:%u",
					 rw_str,
					 (uint32_t)modbus->reg_info_.time_stamp,
					 (uint32_t)modbus->reg_info_.mb_offset,
					 (uint32_t)modbus->reg_info_.type,
					 (uint32_t)modbus->reg_info_.address,
					 (uint32_t)modbus->reg_info_.size);
		}
	}
}

void ModbusSlave::setVoltage(const int id, const uint16_t voltage)
{
	if (id == 1)
	{
		input_reg_params_.voltageL1 = voltage;
	}
	if (id == 2)
	{
		input_reg_params_.voltageL2 = voltage;
	}
	if (id == 3)
	{
		input_reg_params_.voltageL3 = voltage;
	}
}