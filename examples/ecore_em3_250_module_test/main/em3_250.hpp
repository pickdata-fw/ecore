#ifndef EM3250_HPP_
#define EM3250_HPP_

#include "esp_err.h"

esp_err_t setupEM3_250(void);

#endif  // EM3250_HPP_