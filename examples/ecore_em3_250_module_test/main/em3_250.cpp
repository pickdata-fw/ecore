// Mandatory extern "C" headers and forward declarations
//#######################################################
#ifdef __cplusplus
extern "C" {
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_task.h"

#ifdef __cplusplus
}
#endif

#include "EnergyMeter3Module.hpp"
#include "em3_250.hpp"
#include "EmodRet.hpp"
#include "EmodRetMng.hpp"
#include "HAL.hpp"

static const char *TAG = "em3_250";

static EnergyMeter3Module module;

// Constants
static const int32_t kFullScale1 = 1000;
static const int32_t kFullScale2 = 1000;
static const int32_t kFullScale3 = 1000;
static const int32_t kVoltageFs = 230;
static const int32_t kWorkMode = 1;
static const int32_t kMeteringStandard = 1;

// Main module function
esp_err_t setupEM3_250(void) {
    // Initialize module
    if(module.init(ModuleType::typeEM3_250) != EmodRetOk){
        ESP_LOGE(TAG, "could not be initialized!");
		return -1;
	}

    // Configure module
	if(module.configCurrentFullScale(kFullScale1, kFullScale2, kFullScale3) != EmodRetOk){
        ESP_LOGE(TAG, "Current full scale could not be configured!");
		return -1;
	}

    if(module.configVoltageFullScale(kVoltageFs, kVoltageFs, kVoltageFs) != EmodRetOk){
        ESP_LOGE(TAG, "Voltage full scale could not be configured!");
		return -1;
	}

    if(module.configWorkMode((uint16_t) kWorkMode) != EmodRetOk){
        ESP_LOGE(TAG, "Working mode could not be configured!");
		return -1;
	}
   
    if(module.configMeteringStandard((uint16_t)kMeteringStandard) != EmodRetOk){
        ESP_LOGE(TAG, "Metering standard could not be configured!");
		return -1;
	}

    if (module.configCurrentDirection(false, false, false) != EmodRetOk) {
		ESP_LOGE(TAG, "Error, Current direction could not be configured!");
		return -1;
	}

    // Gets the power and energy measurements of phase 1
    PhasePowerParameters power_L1; 
    EnergyParameters energy_L1; 
    while (1) {
        if(module.getPowerParameters(1, &power_L1) == EmodRetOk){
            ESP_LOGI(TAG,"Power-Phase-L1    Voltage=%010.3f V  Current    =%010.3f A    Frequency   =%010.3f Hz    Cosine        =%010.3f        Angle          =%010.3f º\n", power_L1.voltage, power_L1.current, power_L1.frequency, power_L1.cosine, power_L1.angle);
            ESP_LOGI(TAG,"                                        ActivePower=%010.3f kW   AparentPower=%010.3f kVA   ReactivePower =%010.3f kVAr\n", power_L1.active_power, power_L1.aparent_power, power_L1.reactive_power);
            ESP_LOGI(TAG,"                  /*EXPORTED*/          ActivePower=%010.3f kW   AparentPower=%010.3f kVA   InductivePower=%010.3f kVArl  CapacitivePower=%010.3f kVArc\n", power_L1.exported_active_power, power_L1.exported_aparent_power, power_L1.exported_inductive_power, power_L1.exported_capacitive_power);
            ESP_LOGI(TAG,"                  /*IMPORTED*/          ActivePower=%010.3f kW   AparentPower=%010.3f kVA   InductivePower=%010.3f kVArl  CapacitivePower=%010.3f kVArc\n", power_L1.imported_active_power, power_L1.imported_aparent_power, power_L1.imported_inductive_power, power_L1.imported_capacitive_power);
            ESP_LOGI(TAG,"                  MaximumPower(15')=%010.3f kW\n\n", power_L1.maximeter);
        }

        HAL::sleepMs(100);

        if(module.getEnergyParameters(1, &energy_L1) == EmodRetOk){
            ESP_LOGI(TAG,"Energy-Phase-L1                         ActiveEnergy=%010.3f kWh   AparentEnergy=%010.3f kVAh   InductiveEnergy=%010.3f kVArlh  CapacitiveEnergy=%010.3f kVArch\n", energy_L1.active_energy, energy_L1.aparent_energy, energy_L1.inductive_energy, energy_L1.capacitive_energy);
            ESP_LOGI(TAG,"                  /*EXPORTED*/          ActiveEnergy=%010.3f kWh   AparentEnergy=%010.3f kVAh   InductiveEnergy=%010.3f kVArlh  CapacitiveEnergy=%010.3f kVArch\n", energy_L1.exported_active_energy, energy_L1.exported_aparent_energy, energy_L1.exported_inductive_energy, energy_L1.exported_capacitive_energy);
            ESP_LOGI(TAG,"                  /*IMPORTED*/          ActiveEnergy=%010.3f kWh   AparentEnergy=%010.3f kVAh   InductiveEnergy=%010.3f kVArlh  CapacitiveEnergy=%010.3f kVArch\n\n", energy_L1.imported_active_energy, energy_L1.imported_aparent_energy, energy_L1.imported_inductive_energy, energy_L1.imported_capacitive_energy);
        }

        sleepMs(1000);
    }

    return ESP_OK;
}
