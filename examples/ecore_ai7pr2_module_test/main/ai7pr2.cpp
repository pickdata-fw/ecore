// Mandatory extern "C" headers and forward declarations
//#######################################################
#ifdef __cplusplus
extern "C" {
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_task.h"

#ifdef __cplusplus
}
#endif

#include "AInput7Relay2Module.hpp"
#include "EmodRet.hpp"
#include "EmodRetMng.hpp"
#include "HAL.hpp"

#include "ai7pr2.hpp"

static const char *TAG = "ai7pr2";

static AInput7Relay2Module ai7pr2_module;
static TaskHandle_t relay_task;


// relay_task_func function
static void relay_task_func(void *arg) {
    ai7pr2_module.deactivateAll();

    for(;;) {
        for (int i = 0; i < AInput7Relay2Module::ALL_RELAY; i++) {
            ai7pr2_module.activate(1 << i);
            vTaskDelay( 1000 / portTICK_PERIOD_MS );
        }
        ai7pr2_module.deactivateAll();
        vTaskDelay( 1500 / portTICK_PERIOD_MS );
    }
}

static void ai7_callback(const uint8_t* data, uint16_t datalen, uint8_t idFunction, void* ctx) {
    AInput7Relay2Module* module = (AInput7Relay2Module*)ctx;
    uint16_t value;
    int id = 0;
    switch (idFunction) {
        case AInput7Relay2Module::idFunctionINPUT01:
        {
            id = 1;
            break;
        }   
        case AInput7Relay2Module::idFunctionINPUT02:
        {
            id = 2;
            break;
        }
        case AInput7Relay2Module::idFunctionINPUT03:
        {
            id = 3;
            break;
        }
        case AInput7Relay2Module::idFunctionINPUT04:
        {
            id = 4;
            break;
        }
        case AInput7Relay2Module::idFunctionINPUT05:
        {
            id = 5;
            break;
        }
        case AInput7Relay2Module::idFunctionINPUT06:
        {
            id = 6;
            break;
        }
        case AInput7Relay2Module::idFunctionINPUT07:
        {
            id = 7;
            break;
        }
        default:
            ESP_LOGE(TAG, "Unexpected event %d", idFunction);
            break;
    } 

    if(datalen == 2){
        value = (data[0] << 8) + data[1];
        ESP_LOGI(TAG, "Input %d --> voltage=%.2f current=%.2f", id, module->samplesToVoltage(value), module->samplesToCurrent(value));
    }   
}

// SetupRelay function
esp_err_t setupAI7PR2(void) {
    BaseType_t freertos_ret;

    if(ai7pr2_module.init(ai7_callback, &ai7pr2_module) != EmodRetOk){
        ESP_LOGE(TAG, "could not be initialized!");
		return -1;
	}

    if(ai7pr2_module.configSampleRate(250) != EmodRetOk){
        ESP_LOGE(TAG, "could not configure pulse width!");
		return -1;
	}

    // Configure relays pulse width
	if(ai7pr2_module.configPulseWidth(AInput7Relay2Module::ALL_RELAY, 0) != EmodRetOk){
        ESP_LOGE(TAG, "could not configure pulse width!");
		return -1;
	}

    sleepMs(100);

	if(ai7pr2_module.resetEventConfig() != EmodRetOk){
        ESP_LOGE(TAG, "config fails, exiting!");
		return -1;
	}

	if(ai7pr2_module.configEventAtTimeInterval(5000) != EmodRetOk){
        ESP_LOGE(TAG, "config fails, exiting!");
		return -1;
	}

    // Create relay_task_func task
    freertos_ret = xTaskCreate(relay_task_func, "relay_task", 4096, &ai7pr2_module, ESP_TASK_MAIN_PRIO, &relay_task);
    if (freertos_ret != pdPASS) {
        return freertos_ret;
    }

    return ESP_OK;
}
