// Mandatory extern "C" headers and forward declarations
//#######################################################
#ifdef __cplusplus
extern "C" {
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_task.h"

#ifdef __cplusplus
}
#endif

#include "DInput10Module.hpp"
#include "EmodRet.hpp"
#include "EmodRetMng.hpp"
#include "HAL.hpp"

#include "di10.hpp"

static const char *TAG = "di10";

static DInput10Module di10_module;

// di10_callback function
static void di10_callback(const uint8_t* data, uint16_t datalen, uint8_t idFunction, void* ctx) {
    switch (idFunction) {
        case DInput10Module::idFunctionINPUTS:
        {
            bool inputs_status_array[DInput10Module::NUMBER_OF_DI_INPUTS];
            uint16_t inputs_status = data[1];
            for (int i = 0; i < DInput10Module::NUMBER_OF_DI_INPUTS; i++) {
                inputs_status_array[i] = (inputs_status & (1 << i)) != 0;
            }
            ESP_LOGI(TAG, "Inputs status part: [%d, %d, %d, %d, %d, %d, %d, %d, %d, %d]", inputs_status_array[0], inputs_status_array[1], inputs_status_array[2], inputs_status_array[3], inputs_status_array[4],inputs_status_array[5], inputs_status_array[6], inputs_status_array[7], inputs_status_array[8], inputs_status_array[9]);
        }
            break;
        default:
            ESP_LOGE(TAG, "Unexpected event %d", idFunction);
            break;
    }    
}


// SetupRelay function
esp_err_t setupDI10(void) {

    if(di10_module.init(di10_callback, &di10_module) != EmodRetOk){
        ESP_LOGE(TAG, "could not be initialized!");
		return -1;
	}

	uint32_t filter_time = 10;
	if(di10_module.setPulseFilterTime(DInput10Module::DI_ALL_INPUT, filter_time) != EmodRetOk){
        ESP_LOGE(TAG, "could not configure input filter time!");
		return -1;
	}

    sleepMs(100);

	if(di10_module.resetEventConfig() != EmodRetOk){
        ESP_LOGE(TAG, "config fails, exiting!");
		return -1;
	}

	if(di10_module.configEventOnNewData() != EmodRetOk){
        ESP_LOGE(TAG, "config fails, exiting!");
		return -1;
	}

    return ESP_OK;
}
