#ifndef SERIAL_LOG_CONFIG_HPP_
#define SERIAL_LOG_CONFIG_HPP_

#include "esp_err.h"

esp_err_t setupSerialLog(void);

#endif  // SERIAL_LOG_CONFIG_HPP_