function(DOWNLOAD_EMOD_CONTROLLER_COMPONENT FILE_NAME FILE_MD5)

    message(STATUS "Downloading emod_controller libraries ${FILE_NAME}")
    
    file(DOWNLOAD
        https://ecore-libs.s3.eu-west-3.amazonaws.com/${FILE_NAME}
        ${CMAKE_CURRENT_BINARY_DIR}/download/${FILE_NAME}
        TIMEOUT 60  # seconds
        EXPECTED_HASH MD5=${FILE_MD5}
        SHOW_PROGRESS

    )

    file(ARCHIVE_EXTRACT
        INPUT ${CMAKE_CURRENT_BINARY_DIR}/download/${FILE_NAME}
        DESTINATION ${CMAKE_CURRENT_LIST_DIR}/components/emod_controller
    )

    message(STATUS "emod_controller downloaded")

endfunction()
