#ifndef MODBUS_RTU_HPP_
#define MODBUS_RTU_HPP_

#include "esp_err.h"
#include "modbus_params.h"  // for modbus parameters structures
#include "mbcontroller.h"

// The number of parameters that intended to be used in the particular control process
#define MASTER_MAX_CIDS num_device_parameters

// Number of reading of parameters from slave
#define MASTER_MAX_RETRY 30

// Timeout to update cid over Modbus
#define UPDATE_CIDS_TIMEOUT_MS          (500)
#define UPDATE_CIDS_TIMEOUT_TICS        (UPDATE_CIDS_TIMEOUT_MS / portTICK_PERIOD_MS)

// Timeout between polls
#define POLL_TIMEOUT_MS                 (1)
#define POLL_TIMEOUT_TICS               (POLL_TIMEOUT_MS / portTICK_PERIOD_MS)

// The macro to get offset for parameter in the appropriate structure
#define HOLD_OFFSET(field) ((uint16_t)(offsetof(holding_reg_params_t, field) + 1))
#define INPUT_OFFSET(field) ((uint16_t)(offsetof(input_reg_params_t, field) + 1))
#define COIL_OFFSET(field) ((uint16_t)(offsetof(coil_reg_params_t, field) + 1))
// Discrete offset macro
#define DISCR_OFFSET(field) ((uint16_t)(offsetof(discrete_reg_params_t, field) + 1))

#define STR(fieldname) ((const char*)( fieldname ))

#define OPTS(min_val, max_val, step_val) { min_val, max_val, step_val }
// Options can be used as bit masks or parameter limits

static const int32_t kMbPortUart = UART_NUM_1;
static const int32_t kConfigMbUartTxd = 13;
static const int32_t kConfigMbUartRxd = 14;
static const int32_t kConfigMbUartRts = 12;

esp_err_t master_init(void);
void master_operation_func(void *arg);

#endif  // MODBUS_RTU_HPP_