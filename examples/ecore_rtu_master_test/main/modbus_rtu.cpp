/**************************************************************************************************
 * This modbus RTU example is based on the examples provided by ESP-IDF (release/v4.3). For further information,
 * please check the following link: https://github.com/espressif/esp-idf/tree/release/v4.3/examples/protocols/modbus/serial/mb_master
 * ************************************************************************************************/

// extern "C" headers and forward declarations
/////////////////////////////////////////////////////////
#ifdef __cplusplus
extern "C"
{
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "esp_log.h"

#ifdef __cplusplus
}
#endif

#include "modbus_rtu.hpp"

static const char *TAG = "modbus_rtu";

// Example Data (Object) Dictionary for Modbus parameters:
// The CID field in the table must be unique.
// Modbus Slave Addr field defines slave address of the device with correspond parameter.
// Modbus Reg Type - Type of Modbus register area (Holding register, Input Register and such).
// Reg Start field defines the start Modbus register number and Reg Size defines the number of registers for the characteristic accordingly.
// The Instance Offset defines offset in the appropriate parameter structure that will be used as instance to save parameter value.
// Data Type, Data Size specify type of the characteristic and its data size.
// Parameter Options field specifies the options that can be used to process parameter value (limits or masks).
// Access Mode - can be used to implement custom options for processing of characteristic (Read/Write restrictions, factory mode values and etc).
// Example Data Dictionary for Modbus parameters in 2 slaves in the segment
mb_parameter_descriptor_t device_parameters[] = {
    // CID, Name, Units, Modbus addr, register type, Modbus Reg Start Addr, Modbus Reg read length,
    // Instance offset (NA), Instance type, Instance length (bytes), Options (NA), Permissions
    {0, STR("DeviceId"), STR("--"), 1, MB_PARAM_INPUT, 60000, 2, 0, PARAM_TYPE_U16, PARAM_SIZE_U16, {-100, 10, 1000}, PAR_PERMS_READ_WRITE_TRIGGER},
    {1, STR("Start"), STR("--"), 1, MB_PARAM_INPUT, 60117, 2, 0, PARAM_TYPE_U16, PARAM_SIZE_U16, {-100, 10, 1000}, PAR_PERMS_READ_WRITE_TRIGGER},
    {2, STR("Stop"), STR("--"), 1, MB_PARAM_INPUT, 60115, 2, 0, PARAM_TYPE_U16, PARAM_SIZE_U16, {-100, 10, 1000}, PAR_PERMS_READ_WRITE_TRIGGER},
    {3, STR("SetInMaxCharge"), STR("--"), 1, MB_PARAM_INPUT, 60110, 2, 0, PARAM_TYPE_U16, PARAM_SIZE_U16, {-100, 10, 1000}, PAR_PERMS_READ_WRITE_TRIGGER},

};

// Calculate number of parameters in the table
uint16_t num_device_parameters = (sizeof(device_parameters) / sizeof(device_parameters[0]));

// User operation function to read slave values and check alarm
void master_operation_func(void *arg)
{

    esp_err_t err = ESP_OK;
    const mb_parameter_descriptor_t *param_descriptor = NULL;
    uint8_t temp_data[4] = {0};
    uint32_t total = 0;
    uint16_t msb = 0;
    uint16_t lsb = 0;

    ESP_LOGI(TAG, "Start modbus test...");

    for (uint16_t retry = 0; retry <= MASTER_MAX_RETRY; retry++)
    {
        // Read all found characteristics from slave(s)
        for (uint16_t cid = 0; (err != ESP_ERR_NOT_FOUND) && cid < MASTER_MAX_CIDS; cid++)
        {
            // Get data from parameters description table
            // and use this information to fill the characteristics description table
            // and having all required fields in just one table
            err = mbc_master_get_cid_info(cid, &param_descriptor);
            if ((err != ESP_ERR_NOT_FOUND) && (param_descriptor != NULL))
            {
                uint8_t type = 0;
                err = mbc_master_get_parameter(cid, (char *)param_descriptor->param_key, (uint8_t *)temp_data, &type);
                if (err == ESP_OK)
                {
                    ESP_LOGI(TAG, "1: %u // 2: %u // 3: %u // 4: %u", temp_data[0], temp_data[1], temp_data[2], temp_data[3]);
                    if (type == 2)
                    {
                        lsb = temp_data[2] | (temp_data[3] << 8);
                        msb = temp_data[0] | (temp_data[1] << 8);
                        total = lsb | (msb << 16);
                    }
                    else if (type == 1)
                    {
                        lsb = temp_data[0] | (temp_data[1] << 8);
                        total = (uint32_t)lsb;
                    }

                    if ((param_descriptor->mb_param_type == MB_PARAM_HOLDING) || (param_descriptor->mb_param_type == MB_PARAM_INPUT))
                    {
                        ESP_LOGI(TAG, "Characteristic #%d %s (%s) value = %u read successful.",
                                 param_descriptor->cid, (char *)param_descriptor->param_key,
                                 (char *)param_descriptor->param_units, total);
                    }
                    else
                    {

                        ESP_LOGI(TAG, "Characteristic #%d %s (%s) value = %u read successful.",
                                 param_descriptor->cid, (char *)param_descriptor->param_key,
                                 (char *)param_descriptor->param_units, total);
  
                    }
                }
                else
                {
                    ESP_LOGE(TAG, "Characteristic #%d (%s) read fail, err = 0x%x (%s).",
                             param_descriptor->cid, (char *)param_descriptor->param_key,
                             (int)err, (char *)esp_err_to_name(err));
                }
                vTaskDelay(POLL_TIMEOUT_TICS); // timeout between polls
            }
        }
        vTaskDelay(UPDATE_CIDS_TIMEOUT_TICS); //
    }

    /*if (alarm_state)
    {
        ESP_LOGI(TAG, "Alarm triggered by cid #%d.", param_descriptor->cid);
    }
    else
    {
        ESP_LOGE(TAG, "Alarm is not triggered after %d retries.", MASTER_MAX_RETRY);
    }*/
    ESP_LOGI(TAG, "Destroy master...");
    ESP_ERROR_CHECK(mbc_master_destroy());
}

// Modbus master initialization
esp_err_t master_init(void)
{

    // Initialize and start Modbus controller
    mb_communication_info_t comm;
    comm.port = kMbPortUart;
    comm.mode = MB_MODE_RTU;
    comm.baudrate = 9600;
    comm.parity = MB_PARITY_NONE;
    void *master_handler = NULL;

    esp_err_t err = mbc_master_init(MB_PORT_SERIAL_MASTER, &master_handler);
    MB_RETURN_ON_FALSE((master_handler != NULL), ESP_ERR_INVALID_STATE, TAG,
                       "mb controller initialization fail.");
    MB_RETURN_ON_FALSE((err == ESP_OK), ESP_ERR_INVALID_STATE, TAG,
                       "mb controller initialization fail, returns(0x%x).",
                       (uint32_t)err);
    err = mbc_master_setup((void *)&comm);
    MB_RETURN_ON_FALSE((err == ESP_OK), ESP_ERR_INVALID_STATE, TAG,
                       "mb controller setup fail, returns(0x%x).",
                       (uint32_t)err);

    // Set UART pin numbers
    err = uart_set_pin(kMbPortUart, kConfigMbUartTxd, kConfigMbUartRxd,
                       kConfigMbUartRts, UART_PIN_NO_CHANGE);
    MB_RETURN_ON_FALSE((err == ESP_OK), ESP_ERR_INVALID_STATE, TAG,
                       "mb serial set pin failure, uart_set_pin() returned (0x%x).", (uint32_t)err);

    err = mbc_master_start();
    MB_RETURN_ON_FALSE((err == ESP_OK), ESP_ERR_INVALID_STATE, TAG,
                       "mb controller start fail, returns(0x%x).",
                       (uint32_t)err);

    // Set driver mode to Half Duplex
    err = uart_set_mode(kMbPortUart, UART_MODE_RS485_HALF_DUPLEX);
    MB_RETURN_ON_FALSE((err == ESP_OK), ESP_ERR_INVALID_STATE, TAG,
                       "mb serial set mode failure, uart_set_mode() returned (0x%x).", (uint32_t)err);

    vTaskDelay(5);
    err = mbc_master_set_descriptor(&device_parameters[0], num_device_parameters);
    MB_RETURN_ON_FALSE((err == ESP_OK), ESP_ERR_INVALID_STATE, TAG,
                       "mb controller set descriptor fail, returns(0x%x).",
                       (uint32_t)err);
    ESP_LOGI(TAG, "Modbus master stack initialized...");
    return err;
}