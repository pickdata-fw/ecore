// Mandatory extern "C" headers and forward declarations
//#######################################################
#ifdef __cplusplus
extern "C" {
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_task.h"

#ifdef __cplusplus
}
#endif
#include "DInput2Relay1Temp2Module.hpp"
#include "EmodRet.hpp"
#include "EmodRetMng.hpp"
#include "HAL.hpp"

#include "di2t2pr1.hpp"

static const char *TAG = "di2t2pr1";

static DInput2Relay1Temp2Module di2t2pr1_module;

// SetupRelay function
esp_err_t setupDI2T2PR1(void) {
    ESP_LOGI(TAG,"Initializing modulesDi2Pr1T2 ...\n\n");

    if (di2t2pr1_module.init() != 0) {
        ESP_LOGE(TAG, "Module could not be initialized!\n");
        return 1;
    }

    // Configures module parameters
    di2t2pr1_module.configAllPulseWidth(0);
    di2t2pr1_module.setPulseFilterTime(DInput2Relay1Temp2Module::kDigitalInputAll, 10);
    di2t2pr1_module.configAllTempSensor(DInput2Relay1Temp2Module::TempSensorConfig::kTwoWirePt100Sensor);


    while (1) {

        //Test Digital Inputs
        for (int j = 0; j < DInput2Relay1Temp2Module::kNumberOfDigitalInputs; j++) {
            uint16_t mask = 0x0001 << j;
            bool new_data = false;
            uint8_t status;
            uint32_t pulse_count = 0;

            di2t2pr1_module.getStatus(mask, &status, &new_data);
            if (new_data) {
                ESP_LOGI(TAG, "\tNEW VALUE for INPUT %.2d: %d\n", j + 1, status);
            } else {
                ESP_LOGI(TAG, "\tNO CHANGE for INPUT %.2d: %d\n", j + 1, status);
            }

            di2t2pr1_module.getPulseCount(mask, &pulse_count);
            ESP_LOGI(TAG, "Pulse count DI0%d = %d\n", j, pulse_count);
        }

        //Test Temperature Sensors 
        for (uint8_t i = 0; i < DInput2Relay1Temp2Module::kNumberOfTemperatureSensors; i++) {
            DInput2Relay1Temp2Module::TempSensorID proper_id = static_cast<DInput2Relay1Temp2Module::TempSensorID>(1 << i);
            int16_t value;

            di2t2pr1_module.getTempSensor(proper_id, &value);
            ESP_LOGI(TAG, "Temp sensor T0%d = %d\n", i, value);
        }

        //Test Relay
        di2t2pr1_module.activateAll();
        sleepMs(1000);
        di2t2pr1_module.deactivateAll();
        sleepMs(1000);
    }

    return ESP_OK;
}
