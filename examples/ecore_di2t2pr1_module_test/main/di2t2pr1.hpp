#ifndef DI2T2PR1_HPP_
#define DI2T2PR1_HPP_

#include "esp_err.h"

esp_err_t setupDI2T2PR1(void);

#endif  // RELAY_HPP_