#ifndef API_CONFIG_HPP_
#define API_CONFIG_HPP_

#include "esp_err.h"

esp_err_t setupAdminApi(void);

#endif  // API_CONFIG_HPP_