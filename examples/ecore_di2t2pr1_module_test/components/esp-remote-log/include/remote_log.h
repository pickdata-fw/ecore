#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define LOG_FORMAT_BUF_LEN 2048

int remoteLogInit();
int remoteLogFree();

#ifdef __cplusplus
}
#endif