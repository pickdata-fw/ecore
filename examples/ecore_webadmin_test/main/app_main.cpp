// Mandatory extern "C" headers and forward declarations
//#######################################################
#ifdef __cplusplus
extern "C"
{
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "sdkconfig.h"

    // extern app_main() forward declaration
    void app_main();
#ifdef __cplusplus
}
#endif

// Mandatory headers
//#######################################################
#include "api_config.hpp"
#include "serial_log_config.hpp"
#include "webadmin.hpp"
#include "ApiServer.hpp"

// Put your headers here
/////////////////////////////////////////////////////////

// Put your consts here
/////////////////////////////////////////////////////////
static const char *TAG = "app_main";

// Mandatory app_main function
//#######################################################
void app_main(void)
{
    // Mandatory calls
    //#######################################################
    ESP_ERROR_CHECK(setupSerialLog());
    ESP_ERROR_CHECK(setupAdminApi());

    //This function enables telnet logs. You can find more information in https://docs.pickdata.net/ecore/setup/#telnet-logs-configuration
    // ESP_ERROR_CHECK(remoteLogInit());
    // ESP_LOGI(TAG, "Remote log configured!");

    // Put your code here
    /////////////////////////////////////////////////////////
    ApiServer& api_server = ApiServer::getInstance();
    if (api_server.init() != 0) {
		ESP_LOGE(TAG, "Error, ApiServer not initialized properly!");
	}

    ESP_ERROR_CHECK(setupWebadmin());
    ESP_LOGI(TAG, "Serial Log and API configured!");
}
