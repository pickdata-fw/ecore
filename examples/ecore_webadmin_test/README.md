1. First, web submodule initialization is required:
```console
git submodule init
git submodule update
```

2. Install jinja2 package for python3. For example, in ubuntu:
```console
sudo apt install python3-jinja2
```

3. Upload environmental esp-idf variables doing:
```console
source /path/to/esp-idf/export.sh
```

4. Compile web application (React, Angular, Vue, etc.). For this example, do:
```console
cd minimal_web
git checkout develop
npm install
npm run build
cd ..
```

5. Execute the integrate_webadmin_component.sh script:
```console
./integrate_webadmin_component.sh
```

6. Compile project using:
```console
idf.py build
```

Steps from 4 to 6 must be repeated each time the web application is changed. 