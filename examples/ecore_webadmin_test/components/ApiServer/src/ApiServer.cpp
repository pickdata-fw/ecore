#ifdef __cplusplus
extern "C" {
#endif

#include "esp_log.h"
#include "esp_task.h"

#ifdef __cplusplus
}
#endif

#include <time.h>

#include <algorithm>
#include <exception>
#include <nlohmann/json.hpp>
#include <regex>
#include <set>
#include <thread>
#include <utility>

#include "ApiServer.hpp"
#include "HAL.hpp"
#include "HTTPServer.hpp"
#include "NetworkManager.hpp"


static const char* HTTP_TAG = "ApiServer";

static std::set<std::pair<HTTPServerMethod, const char*>> api_urls = {
	{HTTP_SERVER_METHOD_PUT, "/api/example"},
	{HTTP_SERVER_METHOD_GET, "/api/example"},
};

// Default NTP configuration.
static NetworkNTPConfiguration default_ntp_config =
	NetworkNTPConfiguration{.enabled = true, .server = {"0.europe.pool.ntp.org", "1.europe.pool.ntp.org", "2.europe.pool.ntp.org", "3.europe.pool.ntp.org"}};

// Default DNS configuration.
static NetworkDNSConfiguration default_dns_config = {"1.1.1.1", "1.0.0.1", "8.8.8.8", "8.8.4.4"};

// Constructor, Destructor, Init & Uninit
///////////////////////////////////////////////////////////////////////////////////////////////
ApiServer::ApiServer() {
	data_.data_example = "No data saved yet";
}

ApiServer::~ApiServer() {}

EmodRet ApiServer::init() {
	NetworkManager& nm = HAL::getNetworkManager();
	if (nm.init() != 0) {
		return EmodRetErr;
	}

	HTTPServer& http_server = HAL::getHTTPServer();
	if (http_server.init() != 0) {
		return EmodRetErr;
	}

	for (std::set<std::pair<HTTPServerMethod, const char*>>::iterator it = api_urls.begin(); it != api_urls.end(); ++it) {
		if (it->first == HTTP_SERVER_METHOD_GET) {
			http_server.registerHandler(it->first, it->second, getHttpMethod, this);
		} else if (it->first == HTTP_SERVER_METHOD_PUT) {
			http_server.registerHandler(it->first, it->second, putHttpMethod, this);
		}
	}

	// Configure DNS and NTP
	nm.setDNSConfiguration(default_dns_config);
	nm.setNTPConfiguration(default_ntp_config);

	return EmodRetOk;
}

EmodRet ApiServer::uninit() {
	HTTPServer& http_server = HAL::getHTTPServer();

	for (std::set<std::pair<HTTPServerMethod, const char*>>::iterator it = api_urls.begin(); it != api_urls.end(); ++it) {
		http_server.unregisterHandler(it->first, it->second);
	}

	return EmodRetOk;
}

// Getters
///////////////////////////////////////////////////////////////////////////////////////////////
ApiServer& ApiServer::getInstance() {
	static ApiServer instance;
	return instance;
}

DataExample ApiServer::getData() const{
	const std::lock_guard<std::mutex> lock(mutex_);
	return data_;
}

// Setters
///////////////////////////////////////////////////////////////////////////////////////////////
void ApiServer::setData(const DataExample data){
	const std::lock_guard<std::mutex> lock(mutex_);
	data_ = data;
}

// Callbacks
///////////////////////////////////////////////////////////////////////////////////////////////
void ApiServer::getHttpMethod(HTTPRequest& req, void* ctx) {
	ApiServer* api = (ApiServer*)ctx;

	api->requestRateLimit();

	req.setResponseContentType("application/json; charset=utf-8");
	std::string json_response;
	std::string response_http_code = "200 OK";

	if (std::regex_match(req.getUrl(), std::regex("/api/example"))) {

		DataExample data = api->getData();
		json_response = nlohmann::json(data).dump();

	} else {
		response_http_code = "404 Not Found";
		goto exit;
	}

exit:
	req.setResponseStatus(response_http_code);
	req.sendResponse(json_response.c_str(), json_response.length());
	api->last_request_ = std::chrono::steady_clock::now();
}

void ApiServer::putHttpMethod(HTTPRequest& req, void* ctx) {
	ApiServer* api = (ApiServer*)ctx;
	std::string response_http_code = "200 OK";

	api->requestRateLimit();

	req.setResponseContentType("application/json; charset=utf-8");
	std::string json_response;

	char buf[256];
	int content_length = req.getContentLength();
	int content_received = 0;
	int recv_len;
	std::string json_request;

	do {
		recv_len = req.receiveContent(buf, std::min(content_length, (int)sizeof(buf)));
		if (recv_len < 0) {
			goto exit;
		}

		json_request.append(buf, recv_len);

		content_received += recv_len;
	} while (recv_len > 0 && content_received < content_length);

	try {

		if (std::regex_match(req.getUrl(), std::regex("/api/example"))) {
			DataExample data = nlohmann::json::parse(json_request).get<DataExample>();
			api->setData(data);
		} else {
			response_http_code = "404 Not Found";
			goto exit;
		}

	} catch (std::exception& e) {
		response_http_code = "400 Bad Request";
		json_response = std::string("Validation exception: ")+e.what();
		goto exit;
	}

exit:
	req.setResponseStatus(response_http_code);
	req.sendResponse(json_response.c_str(), json_response.length());
	api->last_request_ = std::chrono::steady_clock::now();
}

// Other methods
///////////////////////////////////////////////////////////////////////////////////////////////
void ApiServer::requestRateLimit() {
	auto next_allowed_request = last_request_ + kRequestRateLimitMs;

	if (std::chrono::steady_clock::now() < next_allowed_request) {
		std::this_thread::sleep_until(next_allowed_request);
	}
}
