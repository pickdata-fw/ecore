/****************************************************************************
 * File:    ApiServer.hpp
 * Brief:   This class is able to connect through HTTP protocol.
 * Author:  Laia Seijas
 ***************************************************************************/
#ifndef APISERVER_HPP_
#define APISERVER_HPP_

#include <chrono>
#include <cstdint>
#include <memory>
#include <nlohmann/json.hpp>
#include <mutex>

#include "EmodRet.hpp"
#include "HTTPServer.hpp"


struct DataExample {
	std::string data_example;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(DataExample, data_example);

/**
 * This class implements an example of ApiServer, which establishes communication with the front-end.
 *
 */
class ApiServer {
private:
	// ApiServer attributes 
	///////////////////////////////////////////////////////////////////////////////////////////////
	std::chrono::steady_clock::time_point last_request_;
	
	mutable std::mutex mutex_;
	DataExample data_;

	// Constructor, destructor and methods deleted for a singletone class
	///////////////////////////////////////////////////////////////////////////////////////////////
	ApiServer();
	~ApiServer();
	ApiServer(const ApiServer&) = delete;
	ApiServer& operator=(const ApiServer&) = delete;

	// Methods used to verify if there is a current valid session registered
	///////////////////////////////////////////////////////////////////////////////////////////////
	void requestRateLimit();

	// Callback methods for GET and PUT petitions from the client
	///////////////////////////////////////////////////////////////////////////////////////////////
	static void putHttpMethod(HTTPRequest& req, void* ctx);
	static void getHttpMethod(HTTPRequest& req, void* ctx);

	// Constants
	///////////////////////////////////////////////////////////////////////////////////////////////
	static constexpr std::chrono::milliseconds kRequestRateLimitMs = std::chrono::milliseconds(250);

public:
	// Instantiate and init the ApiServer
	///////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Returns an intsance of the ApiServer class in order to have a singletone.
	 *
	 */
	static ApiServer& getInstance();

	/**
	 * Initialize the ApiServer module.
	 */
	EmodRet init();

	/**
	 * Uninitialize the ApiServer module.
	 *
	 */
	EmodRet uninit();

	DataExample getData() const;

	void setData(const DataExample data);
};

#endif	// APISERVER_HPP_
