#ifndef WEBADMIN_HPP_
#define WEBADMIN_HPP_

#include "esp_err.h"

esp_err_t setupWebadmin(void);

#endif // WEBADMIN_HPP_