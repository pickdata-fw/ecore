// extern "C" headers and forward declarations
/////////////////////////////////////////////////////////
#ifdef __cplusplus
extern "C" {
#endif

// ESP-IDF FreeRTOS headers
#include "freertos/FreeRTOS.h"
#include "esp_log.h"
#include "driver/uart.h"

#ifdef __cplusplus
}
#endif

#include "serial_log_config.hpp"

static const char *TAG = "serial_log";

esp_err_t setupSerialLog(void) {
    // Connect UART1 (consigured as console output in sdkconfig.defaults) to RS232/RS485 peripheral pins
    return uart_set_pin(UART_NUM_1, 13, 14, 12, UART_PIN_NO_CHANGE);
}
