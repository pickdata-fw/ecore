# esp-remote-log

Remote logger server library for ESP32: https://github.com/huming2207/esp-remote-log

## Usage

TL;DR: Run `remote_log_init()` after the network is connected. Then just use `telnet` to connect your ESP32.

At the end, run remote_log_free().

## License

MIT