#!/usr/bin/env python3

import argparse
import jinja2
import os
import sys
import io
import gzip
import shutil
import mimetypes

destination_files_destination_prefix = "files"
gzip_copied_files = True

def generate_code(destination_dir, destination_files_list):
    for root, dirs, files in os.walk(destination_dir):
        # Skip all files inside "destination_files_destination_prefix"
        if os.path.realpath(root).startswith(os.path.join(os.path.realpath(destination_dir), destination_files_destination_prefix)):
            continue
        
        # Search for template files
        for template_filename in files:
            template_file_path = os.path.join(root, template_filename)
            templated_file_path, template_ext = os.path.splitext(template_file_path)
            if template_ext == ".j2":
                # We have a template file

                files = []
                # Generate file list with relative paths to template file
                files_relpath_to_template = []
                for destination_file_path in destination_files_list:
                    files.append({
                        "name": os.path.basename(destination_file_path),
                        "relpath": os.path.relpath(destination_file_path, root),
                        "abspath": os.path.abspath(destination_file_path),
                        "mime_type": mimetypes.guess_type(destination_file_path)[0],
                        "encoding": "gzip" if gzip_copied_files else None,
                        "uri": os.path.relpath(destination_file_path, os.path.join(os.path.realpath(destination_dir), destination_files_destination_prefix)),
                    })
                
                # Process the template file and generate
                env = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=root))
                template = env.get_template(template_filename)
                template.stream(files=files).dump(templated_file_path)


def copy_source_files_to_destination(source_dir, destination_dir):
    destination_files_dir = os.path.join(destination_dir, destination_files_destination_prefix)
    if os.path.exists(destination_files_dir):
        shutil.rmtree(destination_files_dir)
        os.mkdir(destination_files_dir)
    else:
        os.makedirs(destination_files_dir)

    destination_files_list = []

    for root, dirs, files in os.walk(source_dir):
        for dirname in dirs:
            source_dir_path = os.path.join(root, dirname)
            dir_relpath = os.path.relpath(source_dir_path, source_dir)
            os.makedirs(os.path.join(destination_files_dir, dir_relpath))

        for filename in files:
            source_file_path = os.path.join(root, filename)
            file_relpath = os.path.relpath(source_file_path, source_dir)
            destination_file_path = os.path.join(destination_files_dir, file_relpath)

            if gzip_copied_files:
                with open(source_file_path, 'rb') as source_file, gzip.open(destination_file_path, 'wb') as destination_file:
                    shutil.copyfileobj(source_file, destination_file)
                    destination_files_list.append(destination_file_path)
            else:
                shutil.copy(source_file_path, destination_file_path)
    
    return destination_files_list

def main():
    parser = argparse.ArgumentParser(description='Prepare to embed static files in ESP-IDF project')
    parser.add_argument('-s','--source', help='Source directory', required=True)
    parser.add_argument('-d','--destination', help='Destination directory', required=True)
    args = parser.parse_args()

    if not os.path.isdir(args.source):
        sys.stderr.write("source is not a directory\n")
        exit(1)

    if not os.path.isdir(args.destination):
        sys.stderr.write("destination is not a directory\n")
        exit(1)
    
    destination_files_list = copy_source_files_to_destination(os.path.normpath(args.source), os.path.normpath(args.destination))
    generate_code(os.path.normpath(args.destination), destination_files_list)
    
if __name__ == "__main__":
    main()